package com.models;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="\"Loans\"")
@NamedQuery(name="Loans.findAll", query="SELECT l FROM Loans l")
public class Loans implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_loan", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_loan")
	@Column(name="loan_id")
	private Long id;

	@Column(name="days_left")
	private String daysLeft;

	@Column(name="due_date")
	private String dueDate;

	@Column(name="issue_date")
	private String issueDate;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="person_id")
	private Person persons;
		
	//bi-directional many-to-one association to Loan
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="book_id", insertable = false, updatable = false)
	private Books books;
	
	public Loans() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDaysLeft() {
		return this.daysLeft;
	}

	public void setDaysLeft(String daysLeft) {
		this.daysLeft = daysLeft;
	}

	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	@JsonIgnore
	public Person getPersons() {
		return persons;
	}

	public void setPersons(Person persons) {
		this.persons = persons;
	}

	@JsonIgnore
	public Books getBooks() {
		return books;
	}

	public void setBooks(Books books) {
		this.books = books;
	}



}