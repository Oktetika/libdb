package com.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"Books\"")
@NamedQuery(name = "Books.findAll", query = "SELECT t FROM Books t")
public class Books implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "seq_book", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_book")
	@Column(name = "book_id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="publisher_id")
	private Publishers publishers;
	
	@JsonIgnore
	@OneToMany(mappedBy="books")
	@Fetch(FetchMode.JOIN)
	private List<Loans> loans;
	
	@Column(name = "isbn")
	private long isbn;
	
	@Column(name = "book_name")
	private String name;
	
	@Column(name = "book_genre")
	private String bGenre;
	
	@Column(name = "author_fullname")
	
	private String aName;
	
	@Column(name = "on_loan")
	private String onLoan;
	
	@Column(name = "published")
	private String published;
	
	public Books() {
	
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getIsbn() {
		return isbn;
	}
	
	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getbGenre() {
		return bGenre;
	}
	
	public void setbGenre(String bGenre) {
		this.bGenre = bGenre;
	}
	
	public String getaName() {
		return aName;
	}
	
	public void setaName(String aName) {
		this.aName = aName;
	}
	
	public String getOnLoan() {
		return onLoan;
	}
	
	public void setOnLoan(String onLoan) {
		this.onLoan = onLoan;
	}
	
	public String getPublished() {
		return published;
	}
	
	public void setPublished(String published) {
		this.published = published;
	}
	
	public Publishers getPublishers() {
		return publishers;
	}
	
	public void setPublishers(Publishers publishers) {
		this.publishers = publishers;
	}

	public List<Loans> getLoans() {
		return loans;
	}

	public void setLoans(List<Loans> loans) {
		this.loans = loans;
	}
	
}
