package com.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "User_Role" database table.
 * 
 */
@Entity
@Table(name="\"User_Role\"")
@NamedQuery(name="User_Role.findAll", query="SELECT u FROM User_Role u")
public class User_Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_role", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_role")
	@Column(name="role_id")
	private Long roleId;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="username")
	private User user;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="authority" )
	private Role role;
	
	public User_Role() {
	}

	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}