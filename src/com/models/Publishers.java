package com.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"Publishers\"")
@NamedQuery(name = "Publishers.findAll", query = "SELECT t FROM Publishers t")
public class Publishers implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "seq_publisher", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_publisher")
	@Column(name = "publisher_id")	
	private long id;
	@Column(name = "total_book_count")
	private long bCount;
	@Column(name = "publisher_name")
	private String name;
	@Column(name = "publisher_address")
	private String pAddress;
	@Column(name = "publisher_number")
	private String pNumber;
	@OneToMany(mappedBy="publishers")
	private List<Books> books;
	
	public Publishers() {
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@JsonIgnore
	public List<Books> getBooks() {
		return books;
	}

	public void setBooks(List<Books> books) {
		this.books = books;
	}

	public long getbCount() {
		return bCount;
	}

	public void setbCount(long bCount) {
		this.bCount = bCount;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getpAddress() {
		return pAddress;
	}
	
	public void setpAddress(String pAddress) {
		this.pAddress = pAddress;
	}
	
	public String getpNumber() {
		return pNumber;
	}
	
	public void setpNumber(String pNumber) {
		this.pNumber = pNumber;
	}	
}
