package com.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "Role" database table.
 * 
 */
@Entity
@Table(name="\"Role\"")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_auth", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_auth")
	@Column(name="authority_id")
	private Long authorityId;

	//bi-directional many-to-one association to User_Role
	@OneToMany(mappedBy="role")
	private List<User_Role> userRoles;
	
	@Column(name="clearance_level")
	private Long clearanceLevel;

	public Role() {
	}

	public Long getAuthorityId() {
		return this.authorityId;
	}

	public void setAuthorityId(Long authorityId) {
		this.authorityId = authorityId;
	}

	public Long getClearanceLevel() {
		return this.clearanceLevel;
	}

	public void setClearanceLevel(Long clearanceLevel) {
		this.clearanceLevel = clearanceLevel;
	}

	public List<User_Role> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<User_Role> userRoles) {
		this.userRoles = userRoles;
	}

	public User_Role addUserRole(User_Role userRole) {
		getUserRoles().add(userRole);
		userRole.setRole(this);

		return userRole;
	}

	public User_Role removeUserRole(User_Role userRole) {
		getUserRoles().remove(userRole);
		userRole.setRole(null);

		return userRole;
	}

}