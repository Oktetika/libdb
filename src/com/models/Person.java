package com.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.models.Loans;
 
@Entity
@Table(name="\"Person\"")
@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p")
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_user", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user")
	@Column(name="person_id")
	private Long id;

	private String gender;

	private Integer point;

	@Column(name="person_name")
	private String name;

	@Column(name="person_surname")
	private String uSurname;

	//bi-directional many-to-one association to Contact_Info
	@ManyToOne
	@JoinColumn(name="info_id")
	private Contact_Infos contactInfo;
	
	@JsonIgnore
	@OneToMany(mappedBy="persons")
	private List<Loans> loans;

	public Person() {
	
	}
	
	public Person(Long id, String gender, Integer point, String name, String uSurname, Contact_Infos contactInfo) {
		super();
		this.id = id;
		this.gender = gender;
		this.point = point;
		this.name = name;
		this.uSurname = uSurname;
		this.contactInfo = contactInfo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getuSurname() {
		return uSurname;
	}

	public void setuSurname(String uSurname) {
		this.uSurname = uSurname;
	}

	@JsonIgnore
	public Contact_Infos getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(Contact_Infos contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<Loans> getLoans() {
		return loans;
	}

	public void setLoans(List<Loans> loans) {
		this.loans = loans;
	}

	

	
}