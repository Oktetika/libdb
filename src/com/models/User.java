package com.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="\"User\"")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_log", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_log")
	@Column(name="user_id")
	private Long userId;

	//bi-directional many-to-one association to User_Role
	@OneToMany(mappedBy="user")
	private List<User_Role> userRoles;

	private Boolean enabled;

	private String password;

	public User() {
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<User_Role> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<User_Role> userRoles) {
		this.userRoles = userRoles;
	}

	public User_Role addUserRole(User_Role userRole) {
		getUserRoles().add(userRole);
		userRole.setUser(this);

		return userRole;
	}

	public User_Role removeUserRole(User_Role userRole) {
		getUserRoles().remove(userRole);
		userRole.setUser(null);

		return userRole;
	}

}