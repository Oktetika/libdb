package com.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the "Contact_Infos" database table.
 * 
 */
@Entity
@Table(name="\"Contact_Infos\"")
@NamedQuery(name="Contact_Infos.findAll", query="SELECT c FROM Contact_Infos c")
public class Contact_Infos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_user", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user")
	@Column(name="info_id")
	private Long id;

	@Column(name="info_name")
	private String name;
	
	private String address;

	private String city;

	private String email;

	private String number;

	//bi-directional many-to-one association to User
	@JsonIgnore
	@OneToMany(mappedBy="contactInfo")
	private List<Person> users;

	public Contact_Infos() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<Person> getUsers() {
		return this.users;
	}

	public void setUsers(List<Person> users) {
		this.users = users;
	}

	public Person addUser(Person user) {
		getUsers().add(user);
		user.setContactInfo(this);

		return user;
	}

	public Person removeUser(Person user) {
		getUsers().remove(user);
		user.setContactInfo(null);

		return user;
	}

}