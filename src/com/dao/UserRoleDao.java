package com.dao;

import java.util.List;

import com.models.User_Role;

public interface UserRoleDao extends BaseDao<User_Role, Long>{
	List<User_Role> listAll();
}
