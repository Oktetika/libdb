package com.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.models.Contact_Infos;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;

@Repository
public class InfosDaoImpl extends BaseDaoImpl<Contact_Infos, Long> implements InfosDao{

	protected InfosDaoImpl() {
		super(Contact_Infos.class);
	}

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
		public String report(HttpServletResponse response) throws JRException, IOException, SQLException {
			InputStream jasperStream = (InputStream) this.getClass().getResourceAsStream("/com/report/Infos.jasper");
		    Map<String,Object> parameters = new HashMap<String, Object>();
		    Connection c = sessionFactory.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
		  
	        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
	          
		    try {
		    	JRProperties.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
		    	JRProperties.setProperty("net.sf.jasperreports.default.font.name",  "Thoma");
		    	
		    	System.out.println("Report downloaded !");
		    	response.setContentType("application/x-pdf");
		    	response.setHeader("Content-disposition", "inline; filename=infos.pdf");
		     
		    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, c);
		    	final OutputStream outStream = response.getOutputStream();
		    
		    	JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		    }
		    catch (Exception e) {
		    	 System.out.println("Hata : "+e.getMessage());
		} 
		return null;
	}

	@Transactional
	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException {
		InputStream jasperStream = this.getClass().getResourceAsStream("/com/report/infosFiltered.jasper");

	    HashMap<String, Object> map = new HashMap<String, Object>();
	    
	    Connection c = sessionFactory.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
	  
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);

	    map.put("id",id);
	    
	    try {
	    	   JRProperties.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
	    	   JRProperties.setProperty("net.sf.jasperreports.default.font.name", "Thoma");

	    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, c);

	    response.setContentType("application/x-pdf");

	    response.setHeader("Content-disposition", "inline; filename=infosFiltered.pdf");

	    final OutputStream outStream = response.getOutputStream();

	    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

	    }catch (Exception e) {
	    	System.out.println("Hata : "+e.getMessage());
	    } 
	   return null;
	}
}
