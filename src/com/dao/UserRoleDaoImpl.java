package com.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.models.User_Role;

import net.sf.jasperreports.engine.JRException;

@Repository
public class UserRoleDaoImpl extends BaseDaoImpl<User_Role, Long> implements UserRoleDao{
	
	protected UserRoleDaoImpl() {
		super(User_Role.class);
	}

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User_Role> listAll() {
		return sessionFactory.getCurrentSession().createQuery("FROM User_Role")
				.getResultList();
	}
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
