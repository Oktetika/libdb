package com.dao;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;

public interface BaseDao<T extends Serializable, PK extends Serializable> {

	T findById(final PK id);

	T persist(final T entity);

	T update(final T entity);

	T merge(T entity);

	void delete(final T entity);

	void deleteById(final PK id);

	List<T> findAll();
	
	List<T> findLast();

	void findAndDeleteById(final PK id);

	List<T> filterAll(String name);

	T findByEmail(String email);

	List<T> findByRole(T role);

	List<T> filterById(Long id);
	
	List<T> filterBypId(Long id);
	
	public List<T> filterByiId(Long id);
	
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
}
