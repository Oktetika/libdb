package com.dao;

import com.models.User;

public interface UserDao extends BaseDao<User, Long> {
	
}
