package com.dao;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Repository;

import com.models.User;

import net.sf.jasperreports.engine.JRException;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UserDao {

	protected UserDaoImpl() {
		super(User.class);
	}

	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
