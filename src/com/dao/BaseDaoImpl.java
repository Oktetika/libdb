package com.dao;

import com.dao.BaseDao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;
import javax.websocket.Session;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseDaoImpl<T extends Serializable, PK extends Serializable> implements BaseDao<T, PK> {

	private final Class<T> entityClass;

	@Autowired
	private SessionFactory sessionFactory;

	protected BaseDaoImpl(final Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	protected SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	protected Session getCurrentSession() {
		return (Session) this.sessionFactory.getCurrentSession();
	}

	@Override
	public T persist(final T entity) {
		sessionFactory.getCurrentSession().persist(entityClass.getSimpleName(), entity);
		return entity;
	}

	@Override
	public T update(final T entity) {
		sessionFactory.getCurrentSession().update(entityClass.getSimpleName(), entity);
		return entity;
	}

	@Override
	public T merge(final T entity) {
		sessionFactory.getCurrentSession().merge(entityClass.getSimpleName(), entity);
		return entity;
	}

	@Override
	public void delete(final T entity) {
		sessionFactory.getCurrentSession().delete(entityClass.getSimpleName(), entity);
	}

	@Override
	public void findAndDeleteById(PK id) {
		delete(findById(id));
	}

	@Override
	public void deleteById(final PK id) {
		Query q = sessionFactory.getCurrentSession()
				.createQuery("DELETE FROM " + entityClass.getSimpleName() + " t WHERE t.id=:prmId");
		q.setParameter("Id", id);
		q.executeUpdate();
	}

	@Override
	public T findById(final PK id) {
		return sessionFactory.getCurrentSession().find(entityClass, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByRole(final T role) {
		Query q= sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() +" t WHERE t.=:role" , entityClass);
		q.setParameter("role", role);
		return q.getResultList();
	}
	
	@Override
	public T findByEmail(final String email) {
		return sessionFactory.getCurrentSession().find(entityClass, email);
	}
	
	@Override
	public List<T> findAll() {
		return sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() + " ORDER BY id", entityClass)
				.getResultList();
	}
	
	@Override
	public List<T> findLast() {
		return sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() + " ORDER BY id desc LIMIT 5", entityClass)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> filterAll(String name) {
		
		if(name.equals("-1"))
			name = null;
		List<T> filter =(List<T>) sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() + " WHERE (name like '%'|| :name ||'%')").setParameter("name", name).list();
		if(filter.size()>0)
			return filter;
		else
			return null;
		
		/*Query q= sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() +" t WHERE t.name=:name" , entityClass);
		q.setParameter("name", name);
		return q.getResultList();*/
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> filterById(Long id) {
		
		Query q= sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName() +" t WHERE t.id=:id" , entityClass);
		q.setParameter("id", id);
		return q.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> filterBypId(Long id) {
		
		Query q= sessionFactory.getCurrentSession().createQuery("FROM Books t WHERE t.publishers.id=:id");
		q.setParameter("id", id);
		return q.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> filterByiId(Long id) {
		
		Query q= sessionFactory.getCurrentSession().createQuery("FROM Person t WHERE t.contactInfo.id=:id");
		q.setParameter("id", id);
		return q.getResultList();

	}

}
