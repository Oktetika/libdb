package com.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.models.Role;

import net.sf.jasperreports.engine.JRException;

@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role, Long> implements RoleDao{
	
	protected RoleDaoImpl() {
		super(Role.class);
	}

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Role> findAuthorityByName(String username) {
		
		String hql = "select authority from Authorities  where username = :username";
		@SuppressWarnings("unchecked")
		List<Role> result = sessionFactory.getCurrentSession().createQuery(hql)
		.setParameter("username", username)
		.list();
		
		return result;
	/*	sessionFactory.getCurrentSession().createQuery(" authority from authorities WHERE username='admin' ").getSingleResult();
		System.out.println(sessionFactory.getCurrentSession().createQuery(" authority from authorities WHERE username='admin' ").getSingleResult().toString()+"----------------------");
	return	sessionFactory.getCurrentSession().createQuery(" authority from authorities WHERE username='admin' ").getSingleResult().toString();
     */    
		// return (sessionFactory.getCurrentSession().get(Authorities.class, username)).toString();
	}
	
}
