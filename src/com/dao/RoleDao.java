package com.dao;

import java.util.List;

import com.models.Role;

public interface RoleDao extends BaseDao<Role, Long> {
	List<Role> findAuthorityByName(String username);
}
