package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.UserRoleDao;
import com.models.User_Role;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService{

	private final UserRoleDao userRoleDao;
	
	@Autowired
	public UserRoleServiceImpl(UserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}
	
//	@Autowired
//	private UserRoleDao userRoleDao;
	
	@Override
	public List<User_Role> listAll(){
		return userRoleDao.listAll();
	}
}
