package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PublishersDao;
import com.models.Publishers;

import net.sf.jasperreports.engine.JRException;

@Service
@Transactional
public class PublishersServiceImpl implements PublishersService{
	
	@Autowired
	PublishersDao publishersDao;
	
	@Override
	public List<Publishers> findAll(){
		return publishersDao.findAll();
	}
	
	@Override
	public List<Publishers> findLast(){
		return publishersDao.findLast();
	}
	
	@Override
	public List<Publishers> filterAll(String name){
		return publishersDao.filterAll(name);
	}
	
	public List<Publishers> filterById(Long id){
		return publishersDao.filterById(id);
	}
	
	@Override
	public void findAndDeleteById(Long id) {
		publishersDao.findAndDeleteById(id);
	}

	@Override
	public Publishers persist(Publishers publishers) {
		return publishersDao.persist(publishers);
	}
	
	@Override
	public void update(Publishers publishers) {
		publishersDao.update(publishers);
	}

	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException{
		return publishersDao.report(response);
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException{
		return publishersDao.filteredReport(response, id);
	}
	
}
