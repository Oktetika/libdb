package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.models.Person;

import net.sf.jasperreports.engine.JRException;

@Service
public interface PersonService {

	public List<Person> findAll();
	public List<Person> findLast();
	public void findAndDeleteById(Long id);
	public Person persist(final Person users);
	public void update(Person users);
	public List<Person> filterAll(String name);
	public List<Person> filterFkId(Long id);
	public List<Person> filterById(Long id);
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
	
}
