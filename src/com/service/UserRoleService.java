package com.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.models.User_Role;

@Service
public interface UserRoleService {

	List<User_Role> listAll();

}
