package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.models.Books;

import net.sf.jasperreports.engine.JRException;

@Service
public interface BooksService {

	public List<Books> findAll();
	public List<Books> findLast();
	public void deleteById(Long id);
	public Books persist(final Books books);
	public void update(Books books);
	public List<Books> filterAll(String name);
	public List<Books> filterByFkId(Long id);
	public List<Books> filterById(Long id);
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
	
}
