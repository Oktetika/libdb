package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.InfosDao;
import com.models.Contact_Infos;

import net.sf.jasperreports.engine.JRException;;

@Service
@Transactional
public class InfosServiceImpl implements InfosService{
	
	@Autowired
	InfosDao infosDao;
	
	@Override
	public List<Contact_Infos> findAll(){
		return infosDao.findAll();
	}
	
	@Override
	public List<Contact_Infos> findLast(){
		return infosDao.findLast();
	}
	
	@Override
	public List<Contact_Infos> filterAll(String name){
		return infosDao.filterAll(name);
	}
	
	public List<Contact_Infos> filterById(Long id){
		return infosDao.filterById(id);
	}
	
	@Override
	public void findAndDeleteById(Long id) {
		infosDao.findAndDeleteById(id);
	}

	@Override
	public Contact_Infos persist(Contact_Infos infos) {
		return infosDao.persist(infos);
	}
	
	@Override
	public void update(Contact_Infos infos) {
		infosDao.update(infos);
	}
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException{
		return infosDao.report(response);
	}
	
	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException{
		return infosDao.filteredReport(response, id);
	}
	
}
