package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.models.Contact_Infos;

import net.sf.jasperreports.engine.JRException;;

@Service
public interface InfosService {

	public List<Contact_Infos> findAll();
	public List<Contact_Infos> findLast();
	public void findAndDeleteById(Long id);
	public Contact_Infos persist(final Contact_Infos infos);
	public void update(Contact_Infos infos);
	public List<Contact_Infos> filterAll(String name);
	public List<Contact_Infos> filterById(Long id);	
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
	
}
