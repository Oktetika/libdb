package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.BooksDao;
import com.models.Books;

import net.sf.jasperreports.engine.JRException;

@Service
@Transactional
public class BooksServiceImpl implements BooksService{

	@Autowired
	BooksDao booksDao;
	
	@Override
	public List<Books> findAll(){
		return booksDao.findAll();
	}
	
	@Override
	public List<Books> findLast(){
		return booksDao.findLast();
	}

	@Override
	public List<Books> filterAll(String name){
		return booksDao.filterAll(name);
	}
	
	public List<Books> filterByFkId(Long id){
		return booksDao.filterBypId(id);
	}
	
	public List<Books> filterById(Long id){
		return booksDao.filterById(id);
	}
	
	@Override
	public void deleteById(Long id) {
		booksDao.findAndDeleteById(id);
	}
	
	@Override
	public Books persist(Books books) {
		return booksDao.persist(books);
	}
	
	@Override
	public void update(Books books) {
		booksDao.update(books);
	}
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException{
		return booksDao.report(response);
	}
	
	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException{
		return booksDao.filteredReport(response, id);
	}
	
}