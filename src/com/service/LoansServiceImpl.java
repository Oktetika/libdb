package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LoansDao;
import com.models.Loans;

import net.sf.jasperreports.engine.JRException;

@Service
@Transactional
public class LoansServiceImpl implements LoansService{
	
	@Autowired
	LoansDao loansDao;
	
	@Override
	public List<Loans> findAll(){
		return loansDao.findAll();
	}
	
	@Override
	public List<Loans> findLast(){
		return loansDao.findLast();
	}
	
	@Override
	public List<Loans> filterAll(String name){
		return loansDao.filterAll(name);
	}
	
	public List<Loans> filterById(Long id){
		return loansDao.filterById(id);
	}
	
	@Override
	public void findAndDeleteById(Long id) {
		loansDao.findAndDeleteById(id);
	}

	@Override
	public Loans persist(Loans loans) {
		return loansDao.persist(loans);
	}
	
	@Override
	public void update(Loans loans) {
		loansDao.update(loans);
	}
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException{
		return loansDao.report(response);
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException{
		return loansDao.filteredReport(response, id);
	}
	
}
