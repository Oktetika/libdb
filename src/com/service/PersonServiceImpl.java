package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PersonDao;
import com.models.Person;

import net.sf.jasperreports.engine.JRException;

@Service
@Transactional
public class PersonServiceImpl implements PersonService{

	@Autowired
	PersonDao usersDao;
	
	@Override
	public List<Person> findAll(){
		return usersDao.findAll();
	}
	
	@Override
	public List<Person> findLast(){
		return usersDao.findLast();
	}
	
	@Override
	public List<Person> filterAll(String name){
		return usersDao.filterAll(name);
	}
	
	public List<Person> filterFkId(Long id){
		return usersDao.filterByiId(id);
	}
	
	@Override
	public List<Person> filterById(Long id) {
		return usersDao.filterById(id);
	}
	
	@Override
	public void findAndDeleteById(Long id) {
		usersDao.findAndDeleteById(id);
	}

	@Override
	public Person persist(Person users) {
		return usersDao.persist(users);
	}
	
	@Override
	public void update(Person users) {
		usersDao.update(users);
	}
	
	@Override
	public String report(HttpServletResponse response) throws JRException, IOException, SQLException{
		return usersDao.report(response);
	}

	@Override
	public String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException{
		return usersDao.filteredReport(response, id);
	}
	
}