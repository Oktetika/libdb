package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.models.Publishers;

import net.sf.jasperreports.engine.JRException;

@Service
public interface PublishersService {
	
	public List<Publishers> findAll();
	public List<Publishers> findLast();
	public void findAndDeleteById(Long id);
	public Publishers persist(final Publishers publishers);
	public void update(Publishers publishers);
	public List<Publishers> filterAll(String name);
	public List<Publishers> filterById(Long id);
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
	
}
