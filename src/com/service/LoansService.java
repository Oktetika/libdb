package com.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.models.Loans;

import net.sf.jasperreports.engine.JRException;

@Service
public interface LoansService {
	
	public List<Loans> findAll();
	public List<Loans> findLast();
	public void findAndDeleteById(Long id);
	public Loans persist(final Loans loans);
	public void update(Loans loans);
	public List<Loans> filterAll(String name);
	String report(HttpServletResponse response) throws JRException, IOException, SQLException;
	String filteredReport(HttpServletResponse response, long id) throws JRException, IOException, SQLException;
	
}
