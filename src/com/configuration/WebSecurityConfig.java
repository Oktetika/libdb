package com.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("SELECT username, password, enabled FROM public.\"User\" WHERE username=?")
				.authoritiesByUsernameQuery(
						"SELECT username, authority FROM public.\"User_Role\" WHERE username=?")
				.passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
		 	.antMatchers("/resources/**", "/registration"/*, "/rest/**"*/, "/rest/book/**").permitAll()
			.antMatchers("/loan/*").hasAnyRole("LIBRARIAN, MANAGER, ADMIN")
			.antMatchers("/book").hasAnyRole("USER, LIBRARIAN, MANAGER, ADMIN")
				.antMatchers("/book/*").hasAnyRole("LIBRARIAN, MANAGER, ADMIN")
			.antMatchers("/publisher").hasAnyRole("USER, LIBRARIAN, MANAGER, ADMIN")
				.antMatchers("/publisher/*").hasAnyRole("LIBRARIAN, MANAGER, ADMIN")
			.antMatchers("/person/*").hasAnyRole("MANAGER, ADMIN")
			.antMatchers("/info/*").hasAnyRole("MANAGER, ADMIN")
			.antMatchers("/user/*").hasAnyRole("ADMIN")
			.antMatchers("/accessDenied").permitAll()
			.anyRequest().authenticated()
//			.antMatchers("/login").permitAll()
			.and()
				.formLogin()
//					.loginPage("/login")
					.permitAll()
					.defaultSuccessUrl("/")
					.usernameParameter("username").passwordParameter("password")
		.and()
			.exceptionHandling()
			.accessDeniedPage("/accessDenied")
		.and()
			.logout()//.logoutSuccessUrl("/login?logout=true")
			.invalidateHttpSession(true)
			.permitAll().and().csrf().disable();
	}
}
