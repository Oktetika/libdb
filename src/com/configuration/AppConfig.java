package com.configuration;

import org.hibernate.SessionFactory;
import org.postgresql.ds.PGPoolingDataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;


@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "com" })
@EnableTransactionManagement
public class AppConfig implements WebMvcConfigurer{ 

	@Bean
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(getDataSource());
		builder.scanPackages("com.models").addProperties(getHibernateProperties());

		return builder.buildSessionFactory();
	}
	
	private Properties getHibernateProperties() {
		Properties prop = new Properties();
		
		prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", "true");
		prop.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		return prop;
	}
	
	// Create a transaction manager
	@Bean
	public HibernateTransactionManager txManager() {
		return new HibernateTransactionManager(sessionFactory());
	}
	
	@Bean
	public PGPoolingDataSource getDataSource() {

		PGPoolingDataSource source = new PGPoolingDataSource();
		source.setDataSourceName("DataSource");
		source.setServerName("localhost");
		source.setDatabaseName("Library");
		source.setUser("postgres");
		source.setPassword("postgres");
		source.setMaxConnections(10);
		
		Connection conn = null;
		try
		{
		    conn = source.getConnection();
		    // use connection
		}
		catch (SQLException e)
		{
		    // log error
		}
		finally
		{
		    if (conn != null)
		    {
		        try { conn.close(); } catch (SQLException e) {}
		    }
		}
		return source;
	}
	
}




