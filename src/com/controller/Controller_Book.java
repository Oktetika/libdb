package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.models.Books;
import com.service.BooksService;
import com.service.PublishersService;

import net.sf.jasperreports.engine.JRException;

@Controller
public class Controller_Book {

	@Autowired
	BooksService booksService;
	
	@Autowired
	PublishersService publishersService;
	
	//TEST FUNCTION
	/*@RequestMapping(value = "/book", method = RequestMethod.GET)
	public ModelAndView bookFilter(@ModelAttribute(value="q") String q, @ModelAttribute(value="cName") String cName) {
		ModelAndView model = new ModelAndView();
		
		List<Books> filter = null;
		if(q.equals("all")) 
		{
			filter = booksDao.bookList();
		}
		else if(q.isEmpty()) {
			
		}
		else {
			filter = booksDao.bookFilter2(q, cName);
		}
		model.addObject("listBooks", filter);
		model.addObject("filterBooks", booksDao.bookList());
		model.setViewName("books");

		return model;
	}*/
	
	@RequestMapping(value = "/book", method = RequestMethod.GET)
	public ModelAndView bookMain(@ModelAttribute(value="cName") String bName) {
		
		ModelAndView model = new ModelAndView();
		
		List<Books> filter = null;
		
		if(bName.equals("all")) 
		{
			filter = booksService.findAll();
		}
		else if(bName.isEmpty()) {
			filter = booksService.findAll();
//			filter = booksService.findLast();
		}
		else {
			filter = booksService.filterAll(bName);
		}
		
		model.addObject("pubName", publishersService.findAll());
		model.addObject("listBooks", filter);
		model.addObject("filterBooks", booksService.findAll());
		model.setViewName("books");

		return model;
	}
	
	@RequestMapping(value = "/book/publisherBook/{b_id}", method = RequestMethod.GET)
	public ModelAndView bookPublisher(@PathVariable("b_id") long b_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listBooks", booksService.filterByFkId(b_id));
		model.setViewName("books");
		return model;
	}
	
	@RequestMapping(value = "/book/loanBook/{b_id}", method = RequestMethod.GET)
	public ModelAndView bookLoan(@PathVariable("b_id") long b_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listBooks", booksService.filterById(b_id));
		model.setViewName("books");
		return model;
	}
	
	@RequestMapping(value ="/addBook", method = RequestMethod.POST)
	public String bookAdd(Books b, @ModelAttribute(value="p_id") String p_id) {
		booksService.persist(b);
		return ("redirect:/book");
	}

	@RequestMapping(value = "/book/deleteBook/{b_id}", method = RequestMethod.GET)
	public String bookDelete(@PathVariable("b_id") long b_id) {
		booksService.deleteById(b_id);
		return "redirect:/book";
	}
		
	@RequestMapping(value = "/updateBook", method = RequestMethod.POST)
	public String bookUpdate(Books book, @ModelAttribute(value="p_id") String p_id) {
		booksService.update(book);
		return ("redirect:/book");
	}
	
	@ResponseBody
	@RequestMapping(value = "/book/report", method = RequestMethod.GET)
	public void report(HttpServletResponse response) throws JRException, IOException, SQLException {
		booksService.report(response);
	}
	
	@RequestMapping(value = "/book/filteredReport", method = RequestMethod.POST)
	@ResponseBody
	public void filteredReport(HttpServletResponse response,@RequestParam long id) throws JRException, IOException, SQLException {
		booksService.filteredReport(response, id);
	}
	
}
