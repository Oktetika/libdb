package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.models.Publishers;
import com.service.PublishersService;

import net.sf.jasperreports.engine.JRException;

@Controller
public class Controller_Publisher {
	
	@Autowired
	private PublishersService publishersService;
	
	@RequestMapping(value = "/publisher", method = RequestMethod.GET)
	public ModelAndView publisherMain(@ModelAttribute(value="cName") String pName) {
		ModelAndView model = new ModelAndView();
		
		List<Publishers> filter = null;
		
		if(pName.equals("all")) 
		{
			filter = publishersService.findAll();
		}
		else if(pName.isEmpty()) {
			filter = publishersService.findAll();
//			filter = publishersService.findLast();
		}
		else {
			filter = publishersService.filterAll(pName);
		}
		model.addObject("listPublishers", filter);
		model.addObject("filterPublishers", publishersService.findAll());
		model.setViewName("publishers");

		return model;
	}
	
	@RequestMapping(value = "/publisher/bookPublisher/{p_id}", method = RequestMethod.GET)
	public ModelAndView publisherBook(@PathVariable("p_id") long p_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listPublishers", publishersService.filterById(p_id));
		model.setViewName("publishers");
		return model;
	}

	@RequestMapping(value ="/addPublisher", method = RequestMethod.POST)
	public String publisherAdd(Publishers p) {
		publishersService.persist(p);
		return ("redirect:/publisher");
	}

	@RequestMapping(value = "/publisher/deletePublisher/{p_id}", method = RequestMethod.GET)
	public String publisherDelete(@PathVariable("p_id") long p_id) {
		publishersService.findAndDeleteById(p_id);
		return "redirect:/publisher";
	}
		
	@RequestMapping(value = "/updatePublisher", method = RequestMethod.POST)
	public String publisherUpdate(Publishers publisher) {
		publishersService.update(publisher);
		return ("redirect:/publisher");
	}
	
	@ResponseBody
	@RequestMapping(value = "/publisher/report", method = RequestMethod.GET)
	public void report(HttpServletResponse response) throws JRException, IOException, SQLException {
		publishersService.report(response);
	}
	
	@RequestMapping(value = "/publisher/filteredReport", method = RequestMethod.POST)
	@ResponseBody
	public void filteredReport(HttpServletResponse response,@RequestParam long id) throws JRException, IOException, SQLException {
		publishersService.filteredReport(response, id);
	}
	
}
