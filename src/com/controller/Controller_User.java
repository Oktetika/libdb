package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.service.UserRoleService;;

@Controller
public class Controller_User {

	@Autowired
	private UserRoleService userRoleService;
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView userMain(@ModelAttribute(value="cName") String name) {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("listUsers", userRoleService.listAll());
		model.setViewName("users");

		return model;
	}
}
