package com.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.models.Loans;
import com.service.LoansService;

@RestController
@RequestMapping(value = "/rest/loan")
public class Rest_Loans {
	
	@Autowired
	private LoansService loansService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<Loans>> List() throws Exception  {
		
		List<Loans> books = loansService.findAll();
		
		if(books == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok().body(books);
	}
	
	@PostMapping("/add")
	void Create(@RequestBody Loans books) {
		loansService.persist(books);
	}
	
	@PutMapping("/update")
	void Update(@RequestBody Loans loan) {
		loansService.update(loan);
	}
	
	@DeleteMapping("/delete/{id}")
	void Delete(@PathVariable Long id) {
		loansService.findAndDeleteById(id);
	}
}
