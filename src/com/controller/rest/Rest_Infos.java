package com.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.models.Contact_Infos;
import com.service.InfosService;;

@RestController
@RequestMapping(value = "/rest/info")
public class Rest_Infos {

	@Autowired
	private InfosService infosService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<Contact_Infos>> List() throws Exception  {
		
		List<Contact_Infos> books = infosService.findAll();
		
		if(books == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok().body(books);
	}
	
	@PostMapping("/add")
	void Create(@RequestBody Contact_Infos info) {
		infosService.persist(info);
	}
	
	@PutMapping("/update")
	void Update(@RequestBody Contact_Infos info) {
		infosService.update(info);
	}
	
	@DeleteMapping("/delete/{id}")
	void Delete(@PathVariable Long id) {
		infosService.findAndDeleteById(id);
	}
}
