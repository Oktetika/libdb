package com.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.models.Books;
import com.service.BooksService;

@RestController
@RequestMapping(value = "/rest/book")
public class Rest_Book {

	@Autowired
	private BooksService booksService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<Books>> List() throws Exception  {
		
		List<Books> books = booksService.findAll();
		
		if(books == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok().body(books);
	}
	
	@PostMapping(value = "/add")
	public ResponseEntity<?> createCustomer(@RequestBody Books books) {

		booksService.persist(books);

		return new ResponseEntity<Books>(books, HttpStatus.OK);
	}
	
	@PutMapping("/update")
	void Update(@RequestBody Books book) {
		booksService.update(book);
	}
	
	@PutMapping("/add2")
	void Add(@RequestBody Books book) {
		booksService.persist(book);
	}
	
	@DeleteMapping("/delete/{id}")
	void Delete(@PathVariable Long id) {
		booksService.deleteById(id);
	}
}
