package com.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.models.Publishers;
import com.service.PublishersService;;

@RestController
@RequestMapping(value = "/rest/publisher")
public class Rest_Publisher {
	@Autowired
	private PublishersService publishersService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<Publishers>> List() throws Exception  {
		
		List<Publishers> books = publishersService.findAll();
		
		if(books == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok().body(books);
	}
	
	@PostMapping("/add")
	void Create(@RequestBody Publishers publisher) {
		publishersService.persist(publisher);
	}
	
	@PutMapping("/update")
	void Update(@RequestBody Publishers publisher) {
		publishersService.update(publisher);
	}
	
	@DeleteMapping("/delete/{id}")
	void Delete(@PathVariable Long id) {
		publishersService.findAndDeleteById(id);
	}
}
