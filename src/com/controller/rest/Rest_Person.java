package com.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.models.Person;
import com.service.PersonService;;

@RestController
@RequestMapping(value = "/rest/person")
public class Rest_Person {
	@Autowired
	private PersonService personsService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<Person>> List() throws Exception  {
		
		List<Person> books = personsService.findAll();
		
		if(books == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok().body(books);
	}
	
	@PostMapping("/add")
	void Create(@RequestBody Person person) {
		personsService.persist(person);
	}
	
	@PutMapping("/update")
	void Update(@RequestBody Person person) {
		personsService.update(person);
	}
	
	@DeleteMapping("/delete/{id}")
	void Delete(@PathVariable Long id) {
		personsService.findAndDeleteById(id);
	}
}
