package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.models.Loans;
import com.service.LoansService;
import com.service.PersonService;

import net.sf.jasperreports.engine.JRException;

import com.service.BooksService;

@Controller
public class Controller_Loans {
	
	@Autowired
	LoansService loansService;
	@Autowired
	BooksService booksService;
	@Autowired
	PersonService personService;
	
	@RequestMapping(value = "/loan", method = RequestMethod.GET)
	public ModelAndView loanMain(@ModelAttribute(value="cName") String lName) {
		
		ModelAndView model = new ModelAndView();
		
		List<Loans> filter = null;
		
		if(lName.equals("all")) 
		{
			filter = loansService.findAll();
		}
		else if(lName.isEmpty()) {
			filter = loansService.findAll();
//			filter = loansService.findLast();
		}
		else {
			filter = loansService.filterAll(lName);
		}
		
		model.addObject("personName", personService.findAll());
		model.addObject("bookName", booksService.findAll());
		model.addObject("listLoans", filter);
		model.addObject("filterLoans", loansService.findAll());
		model.setViewName("loans");

		return model;
	}
	
	@RequestMapping(value ="/addLoan", method = RequestMethod.POST)
	public String loanAdd(Loans l) {
		loansService.persist(l);
		return ("redirect:/loan");
	}

	@RequestMapping(value = "/loan/deleteLoan/{l_id}", method = RequestMethod.GET)
	public String loanDelete(@PathVariable("l_id") long l_id) {
		loansService.findAndDeleteById(l_id);
		return "redirect:/loan";
	}
		
	@RequestMapping(value = "/updateLoan", method = RequestMethod.POST)
	public String loanUpdate(Loans loan) {
		ModelAndView model = new ModelAndView();
		model.addObject("listLoans", loansService.findAll());
		loansService.update(loan);
		return ("redirect:/loan");
	}
	
	@ResponseBody
	@RequestMapping(value = "/loan/report", method = RequestMethod.GET)
	public void report(HttpServletResponse response) throws JRException, IOException, SQLException {
		loansService.report(response);
	}
	
	@RequestMapping(value = "/loan/filteredReport", method = RequestMethod.POST)
	@ResponseBody
	public void filteredReport(HttpServletResponse response,@RequestParam long id) throws JRException, IOException, SQLException {
		loansService.filteredReport(response, id);
	}
	
}
