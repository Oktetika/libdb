package com.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class TestController {
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ModelAndView homePage(ModelAndView model) throws IOException {

		model.setViewName("register");

		return model;
	}
	

}
