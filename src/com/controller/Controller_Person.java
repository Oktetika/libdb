package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.models.Person;
import com.service.InfosService;
import com.service.PersonService;

import net.sf.jasperreports.engine.JRException;

@Controller
public class Controller_Person {
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private InfosService infosService;
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView personMain(@ModelAttribute(value="cName") String uName) {
		ModelAndView model = new ModelAndView();
		
		List<Person> filter = null;
		
		if(uName.equals("all")) 
		{
			filter = personService.findAll();
		}
		else if(uName.isEmpty()) {
			filter = personService.findAll();
//			filter = personService.findLast();
		}
		else {
			filter = personService.filterAll(uName);
		}
		model.addObject("infoName", infosService.findAll());
		model.addObject("listPerson", filter);
		model.addObject("filterPerson", personService.findAll());
		model.setViewName("person");

		return model;
	}
	
	@RequestMapping(value = "/person/personInfo/{u_id}", method = RequestMethod.GET)
	public ModelAndView personInfo(@PathVariable("u_id") long u_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listPerson", personService.filterFkId(u_id));
		model.setViewName("person");
		return model;
	}
	
	@RequestMapping(value = "/person/personLoan/{u_id}", method = RequestMethod.GET)
	public ModelAndView personLoan(@PathVariable("u_id") long u_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listPerson", personService.filterById(u_id));
		model.setViewName("person");
		return model;
	}
	
	@RequestMapping(value ="/addUser", method = RequestMethod.POST)
	public String personAdd(Person u) {
		personService.persist(u);
		return ("redirect:/user");
	}

	@RequestMapping(value = "/user/deleteUser/{u_id}", method = RequestMethod.GET)
	public String personDelete(@PathVariable("u_id") long u_id) {
		personService.findAndDeleteById(u_id);
		return "redirect:/user";
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public String personUpdate(Person user) {
		personService.update(user);
		return ("redirect:/user");
	}
	
	@ResponseBody
	@RequestMapping(value = "/user/report", method = RequestMethod.GET)
	public void report(HttpServletResponse response) throws JRException, IOException, SQLException {
		personService.report(response);
	}
	
	@RequestMapping(value = "/user/filteredReport", method = RequestMethod.POST)
	@ResponseBody
	public void filteredReport(HttpServletResponse response,@RequestParam long id) throws JRException, IOException, SQLException {
		personService.filteredReport(response, id);
	}
	
}
