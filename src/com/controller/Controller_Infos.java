package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.models.Contact_Infos;
import com.service.InfosService;

import net.sf.jasperreports.engine.JRException;

@Controller
public class Controller_Infos {

	@Autowired
	private InfosService infosService;
	
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public ModelAndView infoMain(@ModelAttribute(value="cName") String iName) {
		ModelAndView model = new ModelAndView();
		
		List<Contact_Infos> filter = null;
		
		if(iName.equals("all")) 
		{
			filter = infosService.findAll();
		}
		else if(iName.isEmpty()) {
			filter = infosService.findAll();
//			filter = infosService.findLast();
		}
		else {
			filter = infosService.filterAll(iName);
		}
		model.addObject("listInfo", filter);
		model.addObject("filterInfo", infosService.findAll());
		model.setViewName("info");

		return model;
	}
	
	@RequestMapping(value = "/info/infoPerson/{i_id}", method = RequestMethod.GET)
	public ModelAndView infoUser(@PathVariable("i_id") long i_id) {
		
		ModelAndView model = new ModelAndView();
		model.addObject("listInfo", infosService.filterById(i_id));
		model.setViewName("info");
		return model;
	}
	
	
	@RequestMapping(value ="/addInfo", method = RequestMethod.POST)
	public String infoAdd(Contact_Infos c) {
		infosService.persist(c);
		return ("redirect:/info");
	}

	@RequestMapping(value = "/info/deleteInfo/{i_id}", method = RequestMethod.GET)
	public String infoDelete(@PathVariable("i_id") long i_id) {
		infosService.findAndDeleteById(i_id);
		return "redirect:/info";
	}
		
	@RequestMapping(value = "/updateInfo", method = RequestMethod.POST)
	public String infoUpdate(Contact_Infos info) {
		infosService.update(info);
		return ("redirect:/info");
	}

	@ResponseBody
	@RequestMapping(value = "/info/report", method = RequestMethod.GET)
	public void report(HttpServletResponse response) throws JRException, IOException, SQLException {
		infosService.report(response);
	}
	
	@RequestMapping(value = "/info/filteredReport", method = RequestMethod.POST)
	@ResponseBody
	public void filteredReport(HttpServletResponse response,@RequestParam long id) throws JRException, IOException, SQLException {
		infosService.filteredReport(response, id);
	}
	
}
