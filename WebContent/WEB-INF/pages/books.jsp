<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>

<title>BOOKS</title>
<%@include file="header.jsp" %>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		</sec:authorize>
  		<a class="active" href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  			<a href="${pageContext.request.contextPath}/info">Contact Information</a>
  		</sec:authorize>
<!--   		<a href="/libdb/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>

	<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
		<div style="display: inline-block"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">New Book</button></div>
	</sec:authorize>
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="book" action="addBook">
    		<div class="container">
      			<h1>New Book</h1>
      			<p>Please fill in this form to add a new book.</p>
	      			<label for="ISBN"><b>ISBN</b></label>
      				<input type="number" placeholder="Enter Book ISBN" name="isbn" required>
      				<label for="name"><b>Name :</b></label>
      				<input type="text" placeholder="Enter Book Name" name="name" required>
      				<label for="genre"><b>Genre :</b></label>
      				<input type="text" placeholder="Enter Book Genre" name="bGenre">
      				<label for="aname"><b>Author Name :</b></label>
      				<input type="text" placeholder="Enter Author Name" name="aName">
      				<label for="loan"><b>On Loan :</b></label>
      				<input type="text" placeholder="Enter Yes/No" name="onLoan">
      				<label for="pub"><b>Published :</b></label>
      				<input type="text" placeholder="Enter Publish Date" name="published">
      				<label for="pname"><b>Publisher Name:</b></label>
      				<select name="publishers.id" required>
	  					<option value="" disabled selected>Select Publisher</option>
  						<c:forEach var="book" items="${pubName}" varStatus="status">
		  					<option value="${book.id}">${book.name}</option>
  						</c:forEach>
  					</select>
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Add Book</button>
      			</div>
    		</div>
  		</form>
	</div>
	
<!-- 	<div style="display: inline-block"> -->
<!-- 		<form action="book" method="get"> -->
<!-- 			<div style="display: inline-block"><input type="text" placeholder="ISBN Search" name="isbn"></div> -->
<!-- 	  		<div style="display: inline-block"><input type="submit" value="Filter"></div>		 -->
<!-- 		</form> -->
<!-- 	</div> -->

    <div style="display: inline-block">
		<form action="book" method="get">
				<div style="display: inline-block">
  					<select name="cName">
  					<option value="" disabled selected>Select your filter option</option>
  					<option value="all">All</option>
  					<c:forEach var="bookFilter" items="${filterBooks}" varStatus="status">
  						<option>${bookFilter.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filter"></div>
		</form>
	</div>

<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">	
	<div style="display: inline-block border:1px; float:right;"><a href="${pageContext.request.contextPath}/book/report">
		<button type="submit" style="width:auto;">Create a Report</button></a></div>
	<br>
	<div style="display: inline-block border:1px; float:right;">
		<form action="${pageContext.request.contextPath}/book/filteredReport" method="post">
				<div style="display: inline-block">
  					<select name="id">
  					<option value="" disabled selected>Choose Book</option>
  					<c:forEach var="bookFilter" items="${filterBooks}" varStatus="status">
  						<option value="${bookFilter.id}">${bookFilter.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filtered Report"></div>
		</form>
	</div>
</sec:authorize>
	
<!-- 	<fieldset> -->
<!-- 	<legend>RAPORLA</legend> -->
<%-- 	<form method="post" action="${pageContext.request.contextPath}/book/filteredReport/${item.id}"> --%>
<!-- 		<table>		 -->
<!-- 			<tr> -->
<!-- 				<td>Book :</td> -->
<!-- 				<td><select name="ogr_id"> -->
<%-- 				<c:forEach items="${listBooks}" var="item"> --%>
<%-- 					<option value="<c:out value="${item.id}" />"><c:out --%>
<%-- 						value="${item.name}" /></option> --%>
<%-- 				</c:forEach> --%>
<!-- 				</select></td>	 -->
<!-- 			<tr> -->
<!-- 		<td><input type="submit" value="Raporla" /></td> -->
<!-- 		</tr> -->
<!-- 		</table> -->
<!-- 	</form>   -->
<!-- 	</fieldset> -->
		
	<div align="center"><h2>BOOKS TABLE TEST</h2></div>

	<div align="center">
		<table border="1">
	
			<tr>
				<th><div align="center">Book ID</div></th>
				<th><div align="center">ISBN</div></th>
				<th><div align="center">Book Name</div></th>
				<th><div align="center">Book Genre</div></th>
				<th><div align="center">Author</div></th>
				<th><div align="center">On Loan</div></th>
				<th><div align="center">Published</div></th>
				<th><div align="center">Publisher ID</div></th>
				<th><div align="center">Option</div></th>
			</tr>
			<c:forEach var="book" items="${listBooks}" varStatus="status">
				<tr>
					<td><div align="center">${book.id}</div></td>
					<td><div align="center">${book.isbn}</div></td>
					<td><div align="center">${book.name}</div></td>
					<td><div align="center">${book.bGenre}</div></td>
					<td><div align="center">${book.aName}</div></td>
					<td><div align="center">${book.onLoan}</div></td>
					<td><div align="center">${book.published}</div></td>
					<td><div align="center">${book.publishers.id}</div></td>
					<td><div align="center">
						<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
							<button onclick="document.getElementById('id02').style.display='block'" onclick="id=${book.id}"	style="width:auto;">Update</button>
						</sec:authorize>	
							<a href="${pageContext.request.contextPath}/publisher/bookPublisher/${book.publishers.id}">
								<button style="width:auto;">Publisher</button></a>
						<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
							<a href="${pageContext.request.contextPath}/book/deleteBook/${book.id}">
								<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></sec:authorize></div></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	


	<div id="id02" class="modal">
		<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="book" action="updateBook">
    		<div class="container">
      			<h1>Update Book</h1>
      			<p>Please fill in this form to update a book.</p>
      				<label for="id"><b>ID :</b></label>
      				<input type="number" placeholder="Enter Book ID" name="id" required>
	      			<label for="ISBN"><b>ISBN :</b></label>
      				<input type="number" placeholder="Enter Book ISBN" name="isbn" required>
      				<label for="name"><b>Name :</b></label>
      				<input type="text" placeholder="Enter Book Name" name="name">
      				<label for="genre"><b>Genre :</b></label>
      				<input type="text" placeholder="Enter Book Genre" name="bGenre">
      				<label for="author"><b>Author Name :</b></label>
      				<input type="text" placeholder="Enter Author Name" name="aName">
      				<label for="loan"><b>On Loan :</b></label>
      				<input type="text" placeholder="Enter Yes/No" name="onLoan">
      				<label for="pub"><b>Published :</b></label>
      				<input type="text" placeholder="Enter Publish Date" name="published">
      				<label for="pname"><b>Publisher Name:</b></label>
      				<select name="publishers.id" required>
	  					<option value="" disabled selected>Select Publisher</option>
  						<c:forEach var="book" items="${listBooks}" varStatus="status">
		  					<option value="${book.publishers.id}">${book.publishers.name}</option>
  						</c:forEach>
  					</select>
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Update Book</button>
      			</div>
      		</div>
      	</form>
	</div>

</body>

</html>
