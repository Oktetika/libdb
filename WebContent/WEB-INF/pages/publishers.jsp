<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>

<title>PUBLISHERS</title>
<%@include file="header.jsp" %>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		</sec:authorize>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a class="active" href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  			<a href="${pageContext.request.contextPath}/info">Contact Information</a>
  		</sec:authorize>
<!--   		<a href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>
	
<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">	
	<div style="display: inline-block"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">New Publisher</button></div>
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="publisher" action="addPublisher">
    		<div class="container">
      			<h1>New Publisher</h1>
      			<p>Please fill in this form to add a new publisher.</p>
      				<label for="name"><b>Name :</b></label>
      				<input type="text" placeholder="Enter Publisher Name" name="name" />
      				<label for="address"><b>Address :</b></label>
      				<input type="text" placeholder="Enter Publisher Address" name="pAddress" />
      				<label for="number"><b>Number :</b></label>
      				<input type="text" placeholder="Enter Publisher Number" name="pNumber" />
      				<label for="count"><b>Book Count :</b></label>
      				<input type="number" placeholder="Enter Publisher Book Count in Library" name="bCount" />
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Add Publisher</button>
      			</div>
    		</div>
  		</form>
	</div>
</sec:authorize>

	<div style="display: inline-block">
		<form action="publisher" method="get">
				<div style="display: inline-block">
  					<select name="cName">
  					<option value="" disabled selected>Select your filter option</option>
  					<option value="all">All</option>
  					<c:forEach var="publisherFilter" items="${filterPublishers}" varStatus="status">
  						<option>${publisherFilter.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filter"></div>
		</form>
	</div>

<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
	<div style="display: inline-block border:1px; float:right;"><a href="${pageContext.request.contextPath}/publisher/report">
		<button type="submit" style="width:auto;">Create a Report</button></a></div>
	<br>
	<div style="display: inline-block border:1px; float:right;">
		<form action="${pageContext.request.contextPath}/publisher/filteredReport" method="post">
				<div style="display: inline-block">
  					<select name="id">
  					<option value="" disabled selected>Choose Publisher</option>
  					<c:forEach var="pub" items="${filterPublishers}" varStatus="status">
  						<option value="${pub.id}">${pub.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filtered Report"></div>
		</form>
	</div>
</sec:authorize>
	
	<div align="center"><h2>PUBLISHERS TABLE TEST</h2></div>

	<div align="center">
	<table border="1">
	
		<tr>
			<th><div align="center">Publisher ID</div></th>
			<th><div align="center">Publisher Name</div></th>
			<th><div align="center">Publisher Address</div></th>
			<th><div align="center">Publisher Number</div></th>
			<th><div align="center">Books in Library</div></th>
			<th><div align="center">Option</div></th>
		</tr>
		<c:forEach var="pub" items="${listPublishers}" varStatus="status">
			<tr>
				<td><div align="center">${pub.id}</div></td>
				<td><div align="center">${pub.name}</div></td>
				<td><div align="center">${pub.pAddress}</div></td>
				<td><div align="center">${pub.pNumber}</div></td>
				<td><div align="center">${pub.bCount}</div></td>
				<td><div align="center">
					<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
						<button onclick="document.getElementById('id02').style.display='block'" onclick="id=${pub.id}"	style="width:auto;">Update</button>
					</sec:authorize>
							<a href="${pageContext.request.contextPath}/book/publisherBook/${pub.id}">
								<button style="width:auto;">Books</button></a>
					<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
							<a href="${pageContext.request.contextPath}/publisher/deletePublisher/${pub.id}">
								<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></sec:authorize></div></td>
			</tr>
		</c:forEach>
	</table>
	</div>

	<div id="id02" class="modal">
		<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="publisher" action="updatePublisher">
    		<div class="container">
      			<h1>Update Book</h1>
      			<p>Please fill in this form to update a publisher.</p>
      				<label for="pid"><b>Publisher ID :</b></label>
      				<input type="number" placeholder="Enter Publisher ID" name="id" required>
      				<label for="name"><b>Name :</b></label>
      				<input type="text" placeholder="Enter Publisher Name" name="name">
      				<label for="address"><b>Address :</b></label>
      				<input type="text" placeholder="Enter Publisher Address" name="pAddress">
      				<label for="number"><b>Number :</b></label>
      				<input type="text" placeholder="Enter Publisher Number" name="pNumber">
      				<label for="count"><b>Book Count :</b></label>
      				<input type="number" placeholder="Enter Publisher Book Count in Library" name="tot_b">
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Update Publisher</button>
      			</div>
      		</div>
      	</form>
	</div>
</body>

</html>
