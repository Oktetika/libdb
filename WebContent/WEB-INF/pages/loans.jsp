<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>

<title>LOANS</title>
<%@include file="header.jsp" %>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<a class="active" href="${pageContext.request.contextPath}/loan">Loans</a>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  			<a href="${pageContext.request.contextPath}/info">Contact Information</a>
  		</sec:authorize>
<!--   		<a href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>

	<div style="display: inline-block"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">New Loan</button></div>
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="loan" action="addLoan">
    		<div class="container">
      			<h1>New Loan</h1>
      			<p>Please fill in this form to add a new book.</p>
      				<label for="person.id"><b>User :</b></label>
      				<select name="person.id" required>
	  					<option value="" disabled selected>Select Borrower</option>
  						<c:forEach var="person" items="${personName}" varStatus="status">
		  					<option value="${person.id}">${person.name}</option>
  						</c:forEach>
  					</select>
  					<label for="book.id"><b>Book :</b></label>
  					<select name="books.id" required>
	  					<option value="" disabled selected>Select Book</option>
  						<c:forEach var="book" items="${bookName}" varStatus="status">
		  					<option value="${book.id}">${book.name}</option>
  						</c:forEach>
  					</select>
  					<br>
      				<label for="issue"><b>Issue Date :</b></label>
      				<input type="text" placeholder="Enter Issue Date" name="issueDate">
      				<label for="due"><b>Due Date :</b></label>
      				<input type="text" placeholder="Enter Due Date" name="dueDate">
      				<label for="dLeft"><b>Days Left :</b></label>
      				<input type="text" placeholder="Enter Days Left" name="daysLeft">
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Add Loan</button>
      			</div>
    		</div>
  		</form>
	</div>
	
	<div style="display: inline-block border:1px; float:right;"><a href="${pageContext.request.contextPath}/loan/report">
		<button type="submit" style="width:auto;">Create a Report</button></a></div>
	<br>
	<div style="display: inline-block border:1px; float:right;">
		<form action="${pageContext.request.contextPath}/loan/filteredReport" method="post">
				<div style="display: inline-block">
  					<select name="id">
  					<option value="" disabled selected>Choose Loan</option>
  					<c:forEach var="loan" items="${filterLoans}" varStatus="status">
  						<option value="${loan.id}">${loan.id}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filtered Report"></div>
		</form>
	</div>
	
	<div align="center"><h2>LOANS TABLE TEST</h2></div>

	<div align="center">
		<table border="1">
	
			<tr>
				<th><div align="center">Loan ID</div></th>
				<th><div align="center">Borrower ID</div></th>
				<th><div align="center">Book ID</div></th>
				<th><div align="center">Issue Date</div></th>
				<th><div align="center">Due Date</div></th>
				<th><div align="center">Days Left</div></th>
				<th><div align="center">Option</div></th>
			</tr>
			<c:forEach var="loan" items="${listLoans}" varStatus="status">
				<tr>
					<td><div align="center">${loan.id}</div></td>
					<td><div align="center">${loan.persons.id}</div></td>
					<td><div align="center">${loan.books.id}</div></td>
					<td><div align="center">${loan.issueDate}</div></td>
					<td><div align="center">${loan.dueDate}</div></td>
					<td><div align="center">${loan.daysLeft}</div></td>
					<td><div align="center"><button onclick="document.getElementById('id02').style.display='block'" onclick="id=${loan.id}"	style="width:auto;">Update</button>
						<a href="${pageContext.request.contextPath}/person/personLoan/${loan.persons.id}">
							<button style="width:auto;">Borrower</button></a>
						<a href="${pageContext.request.contextPath}/book/loanBook/${loan.books.id}">
							<button style="width:auto;">Book</button></a>
						<a href="${pageContext.request.contextPath}/loan/deleteLoan/${loan.id}">
							<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></div></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	


	<div id="id02" class="modal">
		<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="loan" action="updateLoan">
    		<div class="container">
      			<h1>Update Loan</h1>
      			<p>Please fill in this form to update the loan.</p>
  					<label for="id"><b>Loan ID :</b></label>
      				<select name="id" required>
	  					<option value="" disabled selected>Select Borrower</option>
  						<c:forEach var="loan" items="${filterLoans}" varStatus="status">
		  					<option value="${loan.id}">${loan.id}</option>
  						</c:forEach>
  					</select><br>
  					<label for="user.id"><b>Borrower :</b></label>
      				<input type="text" placeholder="Enter Issue Date" name="users.id" required>
      				<label for="book.id"><b>Book :</b></label>
      				<input type="text" placeholder="Enter Issue Date" name="books.id" required>
      				<label for="issue"><b>Issue Date :</b></label>
      				<input type="text" placeholder="Enter Issue Date" name="issueDate" required>
      				<label for="due"><b>Due Date :</b></label>
      				<input type="text" placeholder="Enter Due Date" name="dueDate">
      				<label for="dLeft"><b>Days Left :</b></label>
      				<input type="text" placeholder="Enter Days Left" name="daysLeft">
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Update Loan</button>
      			</div>
      		</div>
      	</form>
	</div>
</body>

</html>
