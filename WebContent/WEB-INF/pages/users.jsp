<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
	<title>USERS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
			<style>
				body {
  					margin: 0;
  					font-family: Arial, Helvetica, sans-serif;
				}

				.topnav {
  					overflow: hidden;
  					background-color: #333;
				}
			
				.topnav a {
  					float: left;
  					color: #f2f2f2;
  					text-align: center;
  					padding: 14px 16px;
  					text-decoration: none;
  					font-size: 17px;
				}
			
				.topnav a:hover {
  					background-color: #ddd;
  					color: black;
				}
			
				.topnav a.active {
  					background-color: #4CAF50;
  					color: white;
				}
			</style>
			
		<style>
			table {
	  			font-family: arial, sans-serif;
  				border-collapse: collapse;
  				width: 100%;
			}

			td, th {
  				border: 1px solid #dddddd;
  				text-align: left;
  				padding: 8px;
			}

			tr:nth-child(even) {
  				background-color: #dddddd;
			}
		</style>
</head>

<style>
	/*SOURCE W3SCHOOLS*/
	body {font-family: Arial, Helvetica, sans-serif;}
	* {box-sizing: border-box;
	}

	/* Full-width input fields */
	input[type=text], input[type=number] {
  		width: 100%;
  		padding: 15px;
  		margin: 5px 0 22px 0;
  		display: inline-block;
  		border: none;
  		background: #f1f1f1;
	}
	
	/* Set a style for all buttons */
	button {
  		background-color: #4CAF50;
  		color: white;
  		padding: 14px 20px;
  		margin: 8px 0;
  		border: none;
  		cursor: pointer;
  		width: 100%;
  		opacity: 0.9;
	}

	button:hover {
  		opacity:1;
	}

	/* Extra styles for the cancel button */
	.cancelbtn {
  		padding: 14px 20px;
  		background-color: #f44336;
	}

	/* Float cancel and signup buttons and add an equal width */
	.cancelbtn, .submitbtn {
  		float: left;
  		width: 50%;
	}

	/* Add padding to container elements */
	.container {
  		padding: 16px;
	}

	/* The Modal (background) */
	.modal {
  		display: none; /* Hidden by default */
  		position: fixed; /* Stay in place */
  		z-index: 1; /* Sit on top */
  		left: 0;
  		top: 0;
  		width: 100%; /* Full width */
  		height: 100%; /* Full height */
  		overflow: auto; /* Enable scroll if needed */
  		background-color: #474e5d;
  		padding-top: 50px;
	}

	/* Modal Content/Box */
	.modal-content {
  		background-color: #fefefe;
  		margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
  		border: 1px solid #888;
  		width: 80%; /* Could be more or less, depending on screen size */
	}

	/* Style the horizontal ruler */
	hr {
  		border: 1px solid #f1f1f1;
  		margin-bottom: 25px;
	}
 
	/* The Close Button (x) */
	.close {
  		position: absolute;
  		right: 35px;
  		top: 15px;
  		font-size: 40px;
  		font-weight: bold;
  		color: #f1f1f1;
	}

	.close:hover,.close:focus {
  		color: #f44336;
  		cursor: pointer;
	}

	/* Clear floats */
	.clearfix::after {
  		content: "";
  		clear: both;
  		display: table;
	}

	/* Change styles for cancel button and signup button on extra small screens */
	@media screen and (max-width: 300px) {
  		.cancelbtn, .submitbtn {
     			width: 100%;
  		}
	}
</style>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  		<a href="${pageContext.request.contextPath}/info">Contact Information</a>
<!--   		<a class="active" href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>
     	
	<div align="center"><h2>USERS TABLE TEST</h2></div>

	<div align="center">
		<table border="1">
	
			<tr>
				<th><div align="center">Username</div></th>
				<th><div align="center">Role</div></th>
				<th><div align="center">Option</div></th>
			</tr>
			<c:forEach var="auth" items="${listUsers}" varStatus="status">
				<tr>
					<td><div align="center">${auth.user.username}</div></td>
					<td><div align="center">${auth.authority}</div></td>
					<td><div align="center"><button onclick="document.getElementById('id02').style.display='block'" onclick="id=${book.id}"	style="width:auto;">Update</button>
						<a href="${pageContext.request.contextPath}/user/deleteUser/${user.username}">
							<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></div></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
</body>

</html>
