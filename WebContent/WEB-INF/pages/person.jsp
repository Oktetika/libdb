<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<title>BORROWERS</title>
<%@include file="header.jsp" %>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<a class="active" href="${pageContext.request.contextPath}/person">Borrowers</a>
  		<a href="${pageContext.request.contextPath}/info">Contact Information</a>
<!--   		<a href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>
	
	<div style="display: inline-block"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">New Borrower</button></div>
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="person" action="addPerson">
    		<div class="container">
      			<h1>New Borrower</h1>
      			<p>Please fill in this form to add a new borrower.</p>
	      			<label for="name"><b>Borrower Name :</b></label>
      				<input type="text" placeholder="Enter Borrower Name" name="name" required>
      				<label for="uSurname"><b>Borrower Surname</b></label>
      				<input type="text" placeholder="Enter Borrower Surname" name="uSurname" required>
      				<label for="gender"><b>Gender</b></label>
      				<input type="text" placeholder="Enter Gender" name="gender" required>
      				<label for="points"><b>Points</b></label>
      				<input type="number" placeholder="Enter Points" name="point" required>
      				<label for="contactInfo"><b>Contact Information</b></label>
      				<select name="contactInfo.id">
	  					<option value="" disabled selected>Select Contact Information ID</option>
  						<c:forEach var="person" items="${infoName}" varStatus="status">
		  					<option value="${person.id}">${person.id}</option>
  						</c:forEach>
  					</select>

      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Add Borrower</button>
      			</div>
    		</div>
  		</form>
	</div>
	
	<div style="display: inline-block">
		<form action="person" method="get">
				<div style="display: inline-block">
  					<select name="cName">
  					<option value="" disabled selected>Select your filter option</option>
  					<option value="all">All</option>
  					<c:forEach var="personFilter" items="${filterPerson}" varStatus="status">
  						<option>${personFilter.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filter"></div>
		</form>
	</div>
	
	<div style="display: inline-block border:1px; float:right;"><a href="${pageContext.request.contextPath}/person/report">
		<button type="submit" style="width:auto;">Create a Report</button></a></div>
	<br>
	<div style="display: inline-block border:1px; float:right;">
		<form action="${pageContext.request.contextPath}/person/filteredReport" method="post">
				<div style="display: inline-block">
  					<select name="id">
  					<option value="" disabled selected>Choose Borrower</option>
  					<c:forEach var="persons" items="${filterPerson}" varStatus="status">
  						<option value="${persons.id}">${persons.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filtered Report"></div>
		</form>
	</div>
	
	<div align="center"><h2>BORROWERS TABLE TEST</h2></div>

	<div align="center">
	<table border="1">
		<tr>
			<th><div align="center">Borrower ID</div></th>
			<th><div align="center">Borrower Name</div></th>
			<th><div align="center">Borrower Surname</div></th>
			<th><div align="center">Gender</div></th>
			<th><div align="center">Points</div></th>
			<th><div align="center">Contact ID</div></th>
			<th><div align="center">Option</div></th>
		</tr>
		<c:forEach var="person" items="${listPerson}" varStatus="status">
			<tr>
				<td><div align="center">${person.id}</div></td>
				<td><div align="center">${person.name}</div></td>
				<td><div align="center">${person.uSurname}</div></td>
				<td><div align="center">${person.gender}</div></td>
				<td><div align="center">${person.point}</div></td>
				<td><div align="center">${person.contactInfo.id}</div></td>
 				<td><div align="center"><button onclick="document.getElementById('id02').style.display='block'" onclick="id=${person.id}"	style="width:auto;">Update</button> 
						<a href="${pageContext.request.contextPath}/info/infoPerson/${person.contactInfo.id}">
							<button style="width:auto;">Info Card</button></a>
						<a href="${pageContext.request.contextPath}/person/deletePerson/${person.id}">
							<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></div></td>
			</tr>
		</c:forEach>
	</table>
	</div>
	
	<div id="id02" class="modal">
		<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="person" action="updatePerson">
    		<div class="container">
      			<h1>Update Person</h1>
      			<p>Please fill in this form to update a borrower.</p>
      				<label for="id"><b>ID :</b></label>
      				<input type="number" placeholder="Enter Borrower ID" name="id" required>
      				<label for="name"><b>Name :</b></label>
      				<input type="text" placeholder="Enter Borrower Name" name="name">
      				<label for="surname"><b>Surname :</b></label>
      				<input type="text" placeholder="Enter Borrower Surname" name="uSurname">
      				<label for="Gender"><b>Gender :</b></label>
      				<input type="text" placeholder="Enter Gender" name="gender">
      				<label for="points"><b>Points :</b></label>
      				<input type="number" placeholder="Enter Points" name="point">
      				<label for="contactInfo"><b>Contact Information</b></label>
      				<select name="contactInfo.id">
	  					<option value="" disabled selected>Select Contact Information ID</option>
  						<c:forEach var="person" items="${listPerson}" varStatus="status" >
		  					<option value="${person.contactInfo.id}">${person.contactInfo.id}</option>
  						</c:forEach>
  					</select>
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Update Borrower</button>
      			</div>
      		</div>
      	</form>
	</div>
	
<!-- 	<div id="id03" class="modal"> -->
<!-- 		<span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span> -->
<!--   		<form class="modal-content" method="get"> -->
<!--     		<div class="container"> -->
<!--       			<h1>Contact Information</h1> -->
<!--       				<table border="1"> -->
<!--       					<tr> -->
<!-- 							<th><div align="center">Contact ID</div></th> -->
<!-- 							<th><div align="center">Email</div></th> -->
<!-- 							<th><div align="center">Address</div></th> -->
<!-- 							<th><div align="center">City</div></th> -->
<!-- 							<th><div align="center">Number</div></th> -->
<!-- 						</tr> -->
<!--  					<c:forEach var="person" items="${listPerson}" varStatus="status"> --%>
<!-- 						<tr> -->
<!-- 							<td><div align="center">${person.contactInfo.id}</div></td> --%>
<!-- 							<td><div align="center">${person.contactInfo.email}</div></td> --%>
<!-- 							<td><div align="center">${person.contactInfo.address}</div></td> --%>
<!-- 							<td><div align="center">${person.contactInfo.city}</div></td> --%>
<!-- 							<td><div align="center">${person.contactInfo.number}</div></td> --%>
<!-- 						</tr> -->
<!--					</c:forEach> -->
<!-- 					</table> -->
<!--       			<div class="clearfix"> -->
<!-- 	        		<button type="button" onclick="document.getElementById('id03').style.display='none'" style="width:auto;" class="cancelbtn">Close</button> -->
<!--       			</div> -->
<!--       		</div> -->
<!--       	</form> -->
<!-- 	</div> -->
	
</body>

</html>
