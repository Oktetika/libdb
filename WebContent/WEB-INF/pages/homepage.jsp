<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>

<html>

<%@include file="header.jsp" %>
<title>HOMEPAGE</title>

<body>

	<div class="topnav">
  		<a class="active" href="${pageContext.request.contextPath}/">Home</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'LIBRARIAN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		</sec:authorize>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<sec:authorize access="hasAnyRole('ADMIN', 'MANAGER')">
  			<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  			<a href="${pageContext.request.contextPath}/info">Contact Information</a>
  		</sec:authorize>
<!--   		<a href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>
	
	<div align="center"><h1>Library Database</h1></div>
	<div align="center"><h2>Homepage</h2></div>
	
</body>
</html>