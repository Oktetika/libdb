<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<title>CONTACT INFORMATION</title>
<%@include file="header.jsp" %>

<body>

	<div class="topnav">
  		<a href="${pageContext.request.contextPath}/">Home</a>
  		<a href="${pageContext.request.contextPath}/loan">Loans</a>
  		<a href="${pageContext.request.contextPath}/book">Books</a>
  		<a href="${pageContext.request.contextPath}/publisher">Publishers</a>
  		<a href="${pageContext.request.contextPath}/person">Borrowers</a>
  		<a class="active" href="${pageContext.request.contextPath}/info">Contact Information</a>
<!--   		<a href="${pageContext.request.contextPath}/user">Users</a> -->
  		<div style="color:red border:1px; float:right;"><a href="${pageContext.request.contextPath}/logout"><font style="color:red">Logout</font></a></div>
	</div>

	<div style="display: inline-block"><button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">New Information Card</button></div>
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="info" action="addInfo">
    		<div class="container">
      			<h1>New Contact Information</h1>
      			<p>Please fill in this form to add a new contact information.</p>
      				<label for="email"><b>Email :</b></label>
      				<input type="text" placeholder="Enter Email" name="email" required>
      				<label for="name"><b>Information Card Name :</b></label>
      				<input type="text" placeholder="Enter Email" name="name" required>
      				<label for="address"><b>Address :</b></label>
      				<input type="text" placeholder="Enter Address" name="address">
      				<label for="city"><b>City :</b></label>
      				<input type="text" placeholder="Enter City" name="city">
      				<label for="number"><b>Phone Number :</b></label>
      				<input type="text" placeholder="Enter Phone Number" name="number">
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Add Contact Information</button>
      			</div>
    		</div>
  		</form>
	</div>
	     
    <div style="display: inline-block">
		<form action="info" method="get">
				<div style="display: inline-block">
  					<select name="cName">
  					<option value="" disabled selected>Select your filter option</option>
  					<option value="all">All</option>
  					<c:forEach var="infoFilter" items="${filterInfo}" varStatus="status">
  						<option>${infoFilter.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filter"></div>
		</form>
	</div>
	
	<div style="display: inline-block border:1px; float:right;"><a href="${pageContext.request.contextPath}/info/report">
		<button type="submit" style="width:auto;">Create a Report</button></a></div>
	<br>
	<div style="display: inline-block border:1px; float:right;">
		<form action="${pageContext.request.contextPath}/info/filteredReport" method="post">
				<div style="display: inline-block">
  					<select name="id">
  					<option value="" disabled selected>Choose Info Card</option>
  					<c:forEach var="info" items="${filterInfo}" varStatus="status">
  						<option value="${info.id}">${info.name}</option>
  					</c:forEach>
  					</select>
		  		</div>
  			<div style="display: inline-block"><input type="submit" value="Filtered Report"></div>
		</form>
	</div>
	<div align="center"><h2>CONTACT INFORMATION TABLE TEST</h2></div>

	<div align="center">
		<table border="1">
	
			<tr>
				<th><div align="center">Info ID</div></th>
				<th><div align="center">Information Card Name</div></th>
				<th><div align="center">Email</div></th>
				<th><div align="center">Address</div></th>
				<th><div align="center">City</div></th>
				<th><div align="center">Phone Number</div></th>
				<th><div align="center">Option</div></th>
			</tr>
			<c:forEach var="info" items="${listInfo}" varStatus="status">
				<tr>
					<td><div align="center">${info.id}</div></td>
					<td><div align="center">${info.name}</div></td>
					<td><div align="center">${info.email}</div></td>
					<td><div align="center">${info.address}</div></td>
					<td><div align="center">${info.city}</div></td>
					<td><div align="center">${info.number}</div></td>
					<td><div align="center"><button onclick="document.getElementById('id02').style.display='block'" onclick="id=${info.id}"	style="width:auto;">Update</button>
						<a href="${pageContext.request.contextPath}/person/personInfo/${info.id}">
							<button style="width:auto;">Borrower</button></a>
						<a href="${pageContext.request.contextPath}/info/deleteInfo/${info.id}">
							<button style="width:auto;"	onclick="return confirm('Are you sure?')">Delete</button></a></div></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<div id="id02" class="modal">
		<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
  		<form class="modal-content" method="post" name="info" action="updateInfo">
    		<div class="container">
      			<h1>Update Book</h1>
      			<p>Please fill in this form to update a contact information.</p>
      				<label for="email"><b>Info ID :</b></label>
      				<input type="number" placeholder="Enter Contact Information ID" name="id" required>
      				<label for="name"><b>Information Card Name :</b></label>
      				<input type="text" placeholder="Enter Email" name="name" required>
      				<label for="email"><b>Email :</b></label>
      				<input type="text" placeholder="Enter Email" name="email" required>
      				<label for="address"><b>Address :</b></label>
      				<input type="text" placeholder="Enter Address" name="address">
      				<label for="city"><b>City :</b></label>
      				<input type="text" placeholder="Enter City" name="city">
      				<label for="number"><b>Phone Number :</b></label>
      				<input type="text" placeholder="Enter Phone Number" name="number">
      			<div class="clearfix">
	        		<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        			<button type="submit" class="submitbtn">Update Contact Information</button>
      			</div>
      		</div>
      	</form>
	</div>
</body>
</html>
