<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<title>USERS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			body {
  			margin: 0;
  			font-family: Arial, Helvetica, sans-serif;
			}

			.topnav {
  			overflow: hidden;
  			background-color: #333;
			}
			
			.topnav a {
  			float: left;
  			color: #f2f2f2;
  			text-align: center;
  			padding: 14px 16px;
  			text-decoration: none;
  			font-size: 17px;
			}
			
			.topnav a:hover {
  			background-color: #ddd;
  			color: black;
			}
			
			.topnav a.active {
  			background-color: #4CAF50;
  			color: white;
			}
		</style>
</head>

<body>

	<div class="topnav">
  		<a href="/libdb/">Home</a>
  		<a href="book">Books</a>
  		<a href="publisher">Publishers</a>
  		<a href="user">Users</a>
  		<a class="active" href="info">Contact Information</a>
	</div>
	
	<div align="center"><h2>INFO TABLE TEST</h2></div>

	<div align="center">
	<table border="1">
	
		<tr>
			<th>Info ID</th>
			<th>User E-Mail</th>
			<th>User Number</th>
			<th>Address</th>
			<th>City</th>
		</tr>
		<c:forEach var="contact" items="${filterInfo}" varStatus="status">
			<tr>
				<td><div align="center">${contact.id}</div></td>
				<td><div align="center">${contact.email}</div></td>
				<td><div align="center">${contact.number}</div></td>
				<td><div align="center">${contact.address}</div></td>
				<td><div align="center">${contact.city}</div></td>

			</tr>
		</c:forEach>
	</table>
	</div>

</body>

</html>
